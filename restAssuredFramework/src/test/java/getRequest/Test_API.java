package getRequest;

import io.restassured.RestAssured;
import io.restassured.response.Response;

import java.awt.List;
import java.util.ArrayList;

import org.json.*;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;


public class Test_API {
	
	private static int code;
	
	public static String testResponse(){
		
		Response resp = null;
		String data = null;
		
		try{
			//making a request to the API URL 
			resp = RestAssured.get("https://api.tmsandbox.co.nz/v1/Categories/6328/Details.json?catalogue=false");
		}catch(Exception e){
			System.out.println("Could not succesfully carry put client request");
			System.out.println("lease reconsider API URL");
		}
		
		//getting API response code
		code = resp.getStatusCode();
		
		//code indicates that the REST API successfully carried out whatever action the client requested
		if(code == 200){
		
			data = resp.asString();
		}
		
		return data;
		
	}
	
	public static boolean isJSONValid(String test) {
	    try {
	        new JSONObject(test);
	    } catch (JSONException ex) {
	        // edited, to include @Arthur's comment
	        // e.g. in case JSONArray is valid as well...
	        try {
	            new JSONArray(test);
	        } catch (JSONException ex1) {
	            return false;
	        }
	    }
	    return true;
	}

	public static void main(String[] args) {

		int plunketIndex = 900;
		
		String data = testResponse();

		if(code == 200) {
			
			//checking if the string is in valid JSON data format 
			if(isJSONValid(data)){
				
				
				//Testing if "Name" key has value "Badges" 
				try{	
					JSONAssert.assertEquals("{\"Name\": \"Badges\"}", data, JSONCompareMode.LENIENT);
					System.out.println("Name = Badges, TEST PASSED");
				}catch(Exception e){
					System.out.println("Name = Badges, TEST FAILED");
				}
				
				//Testing if "CanListClassifieds" key has value "false" 
				try{
					JSONAssert.assertEquals("{\"CanListClassifieds\": false}", data, JSONCompareMode.LENIENT);
					System.out.println("CanListClassifieds = false, TEST PASSED");
				}catch(Exception e){
					System.out.println("CanListClassifieds = false, TEST FAILED");
				}
				
				//Putting the JSON string back in to an object format 
				JSONObject jsonObject = new JSONObject(data);
		
				//JSONArray reference to parse ‘Charities’ array
				JSONArray jsonArray = jsonObject.getJSONArray("Charities");
						
				//looping the Charities JSONarray 
				for(int index = 0 ; index < jsonArray.length() ; index++){
					
					//Checking each different "Description" key
					String test = (jsonArray.getJSONObject(index).getString("Description"));
					
					//if "Description" key has value "Plunket"
					if(test.contains("Plunket")){
						
						System.out.println("Description contains Plunket, TEST PASSED");
						
						//Saving the array index where "Description" key has value "Plunket"
						plunketIndex = index;
					}
					
					
				}
				
				if(plunketIndex == 900){
					
					System.out.println("Description does not contain Plunket, TEST FAILED");
					
				}else{
					
					String test = (jsonArray.getJSONObject(plunketIndex).getString("Tagline"));
					if(test.contains("well child health services")){
						System.out.println("Tagline contains well child health services, TEST PASSED");
					}else{
						System.out.println("Tagline does not contain well child health services, TEST FAILED");
					}
					
				}
				
					
				
			}else{
				System.out.println("API is not in valid JSON format");
			}
			
			
			
		}else{
			System.out.println("Could not succesfully carry put client request");
			System.out.println("lease reconsider API URL");

		}
		

	}

}
